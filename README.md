# ESP32 MPU-6050 Sensor Reading

## Alunos
|Matrícula    | Aluno                              | GitLab                                                     |
| ----------  | ---------------------------------- | ---------------------------------------------------------- |
| 18/0042971  | Natália Schulz Teixeira         | [@Natschteix](https://gitlab.com/Natschteix)                  |
| 22/2022091| Janara Hellen     | [@janarahellen98](https://gitlab.com/janarahellen98)         |


Este projeto demonstra como ler dados de um sensor MPU-6050 usando um ESP32 com o framework ESP-IDF. O MPU-6050 é um sensor de giroscópio e acelerômetro de 6 eixos que pode ser usado para medir a orientação e a aceleração de um dispositivo.

<div align="center">
    <img src="./diagrama_de_dados_instrumet.png"/>    
    <figcaption>Figura 01. Imagem do caminho de dados.</figcaption>

</div>


## Pré-requisitos

Antes de usar este projeto, certifique-se de ter o seguinte configurado:

- Arduino IDE configurado e instalado no seu ambiente de desenvolvimento.

## Conexões

Certifique-se de conectar os pinos SDA e SCL do MPU-6050 aos pinos GPIO 21 e 22, respectivamente, no seu ESP32.

## Como Usar

1. Clone este repositório para o seu ambiente de desenvolvimento Arduino IDE.

```bash
git https://gitlab.com/Natschteix/mpu-electronic_instrumentation.git



