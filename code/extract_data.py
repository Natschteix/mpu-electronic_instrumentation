import numpy as np

gravidade = 1
arquivos = ["y_m1.txt", "y_1.txt", "x_m1.txt", "x_1.txt", "z_m1.txt", "z_1.txt"]

matriz_1 = []
matriz_2 = []
matriz_3 = []

for arquivo_nome in arquivos:
    with open(arquivo_nome, "r") as arquivo:
        linhas = arquivo.readlines()
        valores_linhas = [[float(valor) for valor in linha.split()] for linha in linhas]

    medias_linhas = []
    for linha in valores_linhas:
        media_linha = sum(linha) / len(linha)
        medias_linhas.append(media_linha)

    medias_divididas = [media / gravidade for media in medias_linhas]
    print(f"Médias de cada linha do arquivo {arquivo_nome} divididas pela gravidade:")
    print(medias_divididas)

    if "x_1.txt" in arquivo_nome or "x_m1.txt" in arquivo_nome:
        matriz_1.extend(medias_divididas)
    elif "y_1.txt" in arquivo_nome or "y_m1.txt" in arquivo_nome:
        matriz_2.extend(medias_divididas)
    elif "z_1.txt" in arquivo_nome or "z_m1.txt" in arquivo_nome:
        matriz_3.extend(medias_divididas)

print("Matriz 1:")
print(matriz_1)

print("\nMatriz 2:")
print(matriz_2)

print("\nMatriz 3:")
print(matriz_3)


# Valores da matriz 1
An =  matriz_3[2]
Ap =  matriz_3[5]


# Definindo as equações do sistema
A = np.array([[9.8, 1], [-9.8, 1]])

# Valores à direita das equações
B = np.array([Ap, An])

# Resolvendo o sistema linear
X = np.linalg.solve(A, B)

# Obtendo os valores de Ganho_x e Aoffset_x
Ganho_x = X[0]
Aoffset_x = X[1]


print(f"Valor de Ganho Z: {Ganho_x}")
print(f"Valor de Aoffset Z: {Aoffset_x}")