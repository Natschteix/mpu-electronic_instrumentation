import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error

## Função para carregar dados de um arquivo
def carregar_dados(arquivo):
    with open(arquivo, 'r') as file:
        lines = file.read().splitlines()
        dados = []
        for line in lines:
            valores = line.split()
            for valor in valores:
                dados.append(float(valor))
        return dados

# Carregar dados brutos dos arquivos
dados_eixo_x = carregar_dados('eixo_x.txt')
dados_eixo_y = carregar_dados('eixo_y.txt')
dados_eixo_z = carregar_dados('eixo_z.txt')

# Valores de ganho e offset
offset_x = 0.2101000000000175
ganho_x = 0.9939795918367346

offset_y = -0.1194999999999995
ganho_y = 0.9988775510204064

offset_z = -0.37169999999999437
ganho_z = 1.0169591836734708

# Aplica ganho e offset aos dados brutos
eixo_x_calibrado = np.multiply(np.subtract(dados_eixo_x, offset_x), ganho_x)
eixo_y_calibrado = np.multiply(np.subtract(dados_eixo_y, offset_y), ganho_y)
eixo_z_calibrado = np.multiply(np.subtract(dados_eixo_z, offset_z), ganho_z)

# Salvar os dados compensados em novos arquivos
np.savetxt('eixo_x_compensado.txt', eixo_x_calibrado)
np.savetxt('eixo_y_compensado.txt', eixo_y_calibrado)
np.savetxt('eixo_z_compensado.txt', eixo_z_calibrado)

# Número de amostras
num_amostras = len(dados_eixo_x)

# Plotagem dos gráficos
plt.figure(figsize=(10, 6))

# Gráfico 1 - Eixo X
plt.subplot(3, 1, 1)
plt.plot(np.arange(num_amostras), dados_eixo_x, label='Eixo X')
plt.plot(np.arange(num_amostras), eixo_x_calibrado, label='Eixo X Compensado')
plt.title('Gráfico do Eixo X')
plt.xlabel('Número de Amostras')
plt.ylabel('Graus')
plt.legend()

# Gráfico 2 - Eixo Y
plt.subplot(3, 1, 2)
plt.plot(np.arange(num_amostras), dados_eixo_y, label='Eixo Y')
plt.plot(np.arange(num_amostras), eixo_y_calibrado, label='Eixo Y Compensado')
plt.title('Gráfico do Eixo Y')
plt.xlabel('Número de Amostras')
plt.ylabel('Graus')
plt.legend()

# Gráfico 3 - Eixo Z
plt.subplot(3, 1, 3)
plt.plot(np.arange(num_amostras), dados_eixo_z, label='Eixo Z')
plt.plot(np.arange(num_amostras), eixo_z_calibrado, label='Eixo Z Compensado')
plt.title('Gráfico do Eixo Z')
plt.xlabel('Número de Amostras')
plt.ylabel('Graus')
plt.legend()

plt.tight_layout()
plt.show()

# Calcular o erro quadrático médio (RMSE) para cada eixo
rmse_x = mean_squared_error(dados_eixo_x, eixo_x_calibrado, squared=True)
rmse_y = mean_squared_error(dados_eixo_y, eixo_y_calibrado, squared=True)
rmse_z = mean_squared_error(dados_eixo_z, eixo_z_calibrado, squared=True)

rmse_x_t = np.sqrt(np.mean((eixo_x_calibrado - dados_eixo_x) ** 2))
rmse_y_t = np.sqrt(np.mean((eixo_y_calibrado - dados_eixo_y) ** 2))
rmse_z_t = np.sqrt(np.mean((eixo_z_calibrado - dados_eixo_z) ** 2))

print(f"RMSE para o eixo X: {rmse_x}")
print(f"RMSE para o eixo Y: {rmse_y}")
print(f"RMSE para o eixo Z: {rmse_z}")
