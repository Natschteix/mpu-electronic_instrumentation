import numpy as np

# Definindo as equações do sistema
A = np.array([[-9.530899999999983, 1], [9.951100000000018, 1]])

# Valores à direita das equações
B = np.array([A1x, A2x])

# Resolvendo o sistema linear
X = np.linalg.solve(A, B)

# Obtendo os valores de Ganho_x e Aoffset_x
Ganho_x = X[0]
Aoffset_x = X[1]

print("Valores encontrados:")
print("Ganho_x =", Ganho_x)
print("Aoffset_x =", Aoffset_x)