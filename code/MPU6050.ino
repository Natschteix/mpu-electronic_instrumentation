// Basic demo for accelerometer readings from Adafruit MPU6050

// ESP32 Guide: https://RandomNerdTutorials.com/esp32-mpu-6050-accelerometer-gyroscope-arduino/
// ESP8266 Guide: https://RandomNerdTutorials.com/esp8266-nodemcu-mpu-6050-accelerometer-gyroscope-arduino/
// Arduino Guide: https://RandomNerdTutorials.com/arduino-mpu-6050-accelerometer-gyroscope/

#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

Adafruit_MPU6050 mpu;

// Definir o tamanho máximo das leituras
#define MAX_READINGS 100

// Vetores para armazenar as leituras
float vector_x[MAX_READINGS];
float vector_y[MAX_READINGS];
float vector_z[MAX_READINGS];

// Contador para acompanhar as leituras feitas
int readings_counter = 0;

void setup(void) {
  Serial.begin(115200);
  while (!Serial)
    delay(10); // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit MPU6050 test!");

  // Try to initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Found!");

  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  Serial.print("Accelerometer range set to: ");
  switch (mpu.getAccelerometerRange()) {
  case MPU6050_RANGE_2_G:
    Serial.println("+-2G");
    break;
  case MPU6050_RANGE_4_G:
    Serial.println("+-4G");
    break;
  case MPU6050_RANGE_8_G:
    Serial.println("+-8G");
    break;
  case MPU6050_RANGE_16_G:
    Serial.println("+-16G");
    break;
  }
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  Serial.print("Gyro range set to: ");
  switch (mpu.getGyroRange()) {
  case MPU6050_RANGE_250_DEG:
    Serial.println("+- 250 deg/s");
    break;
  case MPU6050_RANGE_500_DEG:
    Serial.println("+- 500 deg/s");
    break;
  case MPU6050_RANGE_1000_DEG:
    Serial.println("+- 1000 deg/s");
    break;
  case MPU6050_RANGE_2000_DEG:
    Serial.println("+- 2000 deg/s");
    break;
  }

  mpu.setFilterBandwidth(MPU6050_BAND_5_HZ);
  Serial.print("Filter bandwidth set to: ");
  switch (mpu.getFilterBandwidth()) {
  case MPU6050_BAND_260_HZ:
    Serial.println("260 Hz");
    break;
  case MPU6050_BAND_184_HZ:
    Serial.println("184 Hz");
    break;
  case MPU6050_BAND_94_HZ:
    Serial.println("94 Hz");
    break;
  case MPU6050_BAND_44_HZ:
    Serial.println("44 Hz");
    break;
  case MPU6050_BAND_21_HZ:
    Serial.println("21 Hz");
    break;
  case MPU6050_BAND_10_HZ:
    Serial.println("10 Hz");
    break;
  case MPU6050_BAND_5_HZ:
    Serial.println("5 Hz");
    break;
  }

  Serial.println("");
  delay(100);
}

void loop() {
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  // Verificar se ainda não foram feitas 100 leituras
  if (readings_counter < MAX_READINGS) {
    vector_x[readings_counter] = a.acceleration.x;
    vector_y[readings_counter] = a.acceleration.y;
    vector_z[readings_counter] = a.acceleration.z;

    readings_counter++; // Incrementar o contador de leituras
  }

  // Se atingiu 100 leituras, imprimir os valores e reiniciar
  if (readings_counter == MAX_READINGS) {
    Serial.println("Valores dos vetores:");
    Serial.print("Vetor x = ");
    for (int i = 0; i < MAX_READINGS; i++) {
      Serial.print(vector_x[i]);
      Serial.print(" ");
    }
    Serial.println();

    Serial.print("Vetor y = ");
    for (int i = 0; i < MAX_READINGS; i++) {
      Serial.print(vector_y[i]);
      Serial.print(" ");
    }
    Serial.println();

    Serial.print("Vetor z = ");
    for (int i = 0; i < MAX_READINGS; i++) {
      Serial.print(vector_z[i]);
      Serial.print(" ");
    }
    Serial.println();

    // Reiniciar contadores e vetores para novas leituras
    readings_counter = 0;
    for (int i = 0; i < MAX_READINGS; i++) {
      vector_x[i] = 0;
      vector_y[i] = 0;
      vector_z[i] = 0;
    }

    delay(5000); // Delay para visualização dos dados no Serial Monitor
  }
}
